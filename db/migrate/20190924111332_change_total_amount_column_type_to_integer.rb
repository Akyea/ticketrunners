class ChangeTotalAmountColumnTypeToInteger < ActiveRecord::Migration[6.0]
  def change
    change_column :bookings, :total_amount, :integer
  end
end
