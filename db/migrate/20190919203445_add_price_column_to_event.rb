# frozen_string_literal: true

class AddPriceColumnToEvent < ActiveRecord::Migration[5.2]
  def change
    add_column :events, :price, :decimal
  end
end
