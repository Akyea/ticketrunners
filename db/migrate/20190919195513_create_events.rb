# frozen_string_literal: true

class CreateEvents < ActiveRecord::Migration[5.2]
  def change
    create_table :events do |t|
      t.string :name
      t.string :venue
      t.datetime :starts_at
      t.integer :alloted_tickets
      t.integer :tickets_sold

      t.timestamps
    end
  end
end
