# frozen_string_literal: true

class CreateBookings < ActiveRecord::Migration[5.2]
  def change
    create_table :bookings do |t|
      t.integer :user_id
      t.integer :event_id
      t.decimal :price
      t.integer :number_of_tickets

      t.timestamps
    end
  end
end
