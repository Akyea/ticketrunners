# frozen_string_literal: true

class RenameColumnInBooking < ActiveRecord::Migration[5.2]
  def change
    rename_column :bookings, :price, :total_amount
  end
end
