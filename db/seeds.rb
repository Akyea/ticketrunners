# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.create!(email: "stanley@bolad.com", name: "Stanley Bolad", admin: true)
User.create!(email: "monika@bolad.com", name: "Monika Zych", admin: false)
User.create!(email: "henryk@bolad.com", name: "Henryk Akyea", admin: false)

Event.create!(name: 'Champions League Finals', venue: "Old Trafford", starts_at: Time.now, alloted_tickets: 1500, tickets_sold: 0, price: 200)
Event.create!(name: 'Oktoberfest', venue: "Munich", starts_at: Time.now, alloted_tickets: 500, tickets_sold: 0, price: 100)
Event.create!(name: 'Chalewote Street Art', venue: "High Streets",  starts_at: Time.now, alloted_tickets: 1000, tickets_sold: 0, price: 500)
