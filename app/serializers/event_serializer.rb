class EventSerializer < ActiveModel::Serializer
  attributes :id, :name, :venue, :alloted_tickets, :tickets_sold, :start_date, :start_time

  has_many :bookings
  has_many :users

  def start_date
    object.starts_at.strftime("%d.%m.%Y")
  end

  def start_time
    object.starts_at.strftime("%I:%M%p")
  end
end
