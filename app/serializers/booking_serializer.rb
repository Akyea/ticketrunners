class BookingSerializer < ActiveModel::Serializer
  attributes :id, :number_of_tickets, :total_amount

  belongs_to :user
  belongs_to :event
end
