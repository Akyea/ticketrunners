# frozen_string_literal: true

class Api::EventsController < ApplicationController

  before_action :set_event, only: %i[show available_tickets]
  before_action :require_admin, only: [:update, :destroy]

  def index
    @events = Event.all
    render json: @events, status: :ok
  end

  def show
    render json: @event
  end

  def create
    @event = Event.new(event_params)
    @event.save
    render json: @event, status: :ok
  end

  def update
    @event.update(event_params)
    render json: @event, status: :ok
  end

  def destroy
    @event = Event.find(params[:id])
    @event.destroy
    head :no_content
  end

  def available_tickets
    available_tickets = @event.alloted_tickets - @event.tickets_sold
    render json: { available_tickets: available_tickets.negative? ? 0 : available_tickets }
  end

  private

  def set_event
    @event = Event.find(params[:id])
  end

  def event_params
    params.require(:event).permit(:name, :venue, :starts_at, :alloted_tickets, :tickets_sold, :price)
  end

end
