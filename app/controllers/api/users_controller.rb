# frozen_string_literal: true

class Api::UsersController < ApplicationController

  before_action :set_user, only: [:show, :destroy]
  before_action :require_admin, only: [:destroy]

  def index
    @users = User.all
    render json: @users, status: :ok
  end

  def new
    @user = User.new
  end

  def show

  end

  def create
    @user = User.new(user_params)
    @user.save
    render json: @user, status: :ok
  end

  def destroy
    @user.destroy
    head :no_content
  end

  private

  def set_user
    @user = User.find(params[:id])
  end

  def user_params
    params.require(:user).permit(:name, :email)
  end

end
