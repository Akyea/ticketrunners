# frozen_string_literal: true

class Api::BookingsController < ApplicationController

  before_action :set_event
  before_action :check_ticket_availability

  def create
    @booking = Booking.new(booking_params)
    @booking.user = current_user

    if @booking.valid?
      begin
        payment = Adapters::Payment::Gateway.charge(
          amount: @booking.compute_purchase_price(gateway_params[:amount].to_i, @booking.number_of_tickets),
          token: gateway_params[:token],
          currency: gateway_params[:currency]
        )
        @booking.total_amount = payment.amount
        @booking.save
        render json: @booking, status: :created

      rescue Adapters::Payment::Gateway::CardError => e
        Rails.logger.info("Booking failed: #{e.message}")
        render json: {error: e.message}, status: :unprocessable_entity
      rescue Adapters::Payment::Gateway::PaymentError => e
        Rails.logger.info("Booking failed: #{e.message}")
        render json: {error: e.message}, status: :unprocessable_entity
      end
    else
      render json: {message: "Could not process your booking. Please try again!"}, status: :unprocessable_entity
    end
  end

  private

  #set the parameter as permitted and limit which attributes should be allowed for mass updating
  def booking_params
    params.require(:bookings).permit(:number_of_tickets, :event_id)
  end

  def gateway_params
    params.require(:bookings).permit(:amount, :token, :currency)
  end

  def set_event
    @event = Event.find(booking_params[:event_id])
  end

  def check_ticket_availability
    if @event.tickets_sold >= @event.alloted_tickets
      render(json: { error: 'Event sold out' }, status: :unprocessable_entity) && return
    end
  end
end
