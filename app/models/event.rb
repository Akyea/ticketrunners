# frozen_string_literal: true

class Event < ApplicationRecord
  has_many :bookings, dependent: :destroy
  has_many :users, through: :bookings

  validates_presence_of :name, :venue, :starts_at
end
