# frozen_string_literal: true

class Booking < ApplicationRecord
  belongs_to :event
  belongs_to :user

  after_create :reduce_tickets
  after_destroy :release_tickets

  def compute_purchase_price(amount, tickets)
    amount * tickets
  end

  private

  # helps check how many tickets remaining for an event
  def release_tickets
    tickets_to_release = number_of_tickets
    event = self.event
    event.update(tickets_sold: event.tickets_sold - tickets_to_release)
  end

  # helps check how many tickets have been sold out for an event
  def reduce_tickets
    consumable_tickets = number_of_tickets
    event = self.event
    event.update(tickets_sold: event.tickets_sold + consumable_tickets)
  end

end
