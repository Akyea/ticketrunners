# frozen_string_literal: true

Rails.application.routes.draw do
  namespace :api do
    resources :events do
      get 'available-tickets', action: :available_tickets, on: :member
    end
    resources :bookings, only: [:create]
    resources :users
  end
end
