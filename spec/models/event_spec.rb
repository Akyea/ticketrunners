# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Event, type: :model do
  describe '#validations' do
    it { should have_many(:bookings).dependent(:destroy) }
    it { should validate_presence_of(:name) }
    it { should validate_presence_of(:venue) }
    it { should validate_presence_of(:starts_at) }
  end
end
