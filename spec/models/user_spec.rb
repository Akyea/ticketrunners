# frozen_string_literal: true

require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#validations' do
    it 'should validate the presence of attributes' do
      user = build :user, name: '', email: ''
      expect(user).not_to be_valid
      expect(user.errors.messages[:name]).to include("can't be blank")
      expect(user.errors.messages[:email]).to include("can't be blank")
    end

    it 'should validate the uniqueness of attributes' do
      user1 = create :user
      user2 = build :user, name: user1.name, email: user1.email
      expect(user2).not_to be_valid

      user2.name = 'Monika Akyea'
      user2.email = 'makyea@myspace.com'
      expect(user2).to be_valid
    end

    it { should have_many(:bookings) }
  end
end
