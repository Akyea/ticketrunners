# frozen_string_literal: true

require 'rails_helper'

RSpec.describe 'Bookings', type: :request do
  describe 'POST create' do
    let(:event) { create :event }
    let(:tickets) { 5 }
    let(:user) { create :user }

    it 'attemps to buy tickets for a sold out event' do
      event.bookings.create(user_id: user.id, number_of_tickets: 10, total_amount: 2000)

      post '/api/bookings/',
           params: {
             bookings: {
               user_id: user.id,
               event_id: event.id,
               number_of_tickets: tickets,
               amount: event.price,
               currency: 'EUR',
               token: :payment_done
             },
             format: :json
           },
           headers: { "X-User-Email": user.email }

      expect(response).to be_unprocessable
      expect(response.content_type).to eq("application/json")
      expect(json['error']).to eq('Event sold out')
    end

    it 'attemps to buy 5 tickets with an invalid card' do
      post '/api/bookings/',
        params: {
          bookings: {
            event_id: event.id,
            number_of_tickets: tickets,
            amount: event.price,
            currency: 'EUR',
            token: :card_error
          },
          format: :json
        },
        headers: { "X-User-Email": user.email }

    expect(response).to be_unprocessable
    expect(response.content_type).to eq("application/json")
    expect(json['error']).to eq('Your card has been declined.')
    end

    it 'attemps to buy 5 tickets with failed payment' do
      post '/api/bookings/',
        params: {
          bookings: {
            event_id: event.id,
            number_of_tickets: tickets,
            amount: event.price,
            currency: 'EUR',
            token: :payment_error
          },
          format: :json
        },
        headers: { "X-User-Email": user.email }

    expect(response).to be_unprocessable
    expect(response.content_type).to eq("application/json")
    expect(json['error']).to eq('Something went wrong with your transaction.')
    end

    it 'attemps to buy tickets for an non-existing event' do
      expect {
        post '/api/bookings/',
          params: {
            bookings: {
              event_id: "45232424",
              number_of_tickets: tickets,
              amount: event.price,
              currency: 'EUR',
              token: :payment_done
            },
            format: :json
          },
          headers: { "X-User-Email": user.email }
        }.to raise_error(ActiveRecord::RecordNotFound)
    end

    it 'should succesfully purchase 5 tickets' do
      post '/api/bookings/',
        params: {
          bookings: {
            event_id: event.id,
            number_of_tickets: tickets,
            amount: event.price,
            currency: 'EUR',
            token: :payment_done
          },
          format: :json
        },
        headers: { "X-User-Email": user.email }

    expect(response).to be_created
    expect(response.content_type).to eq("application/json")
    expect(json['id']).to be_present
    expect(json['total_amount']).to eq(event.price * tickets)
    expect(json['number_of_tickets']).to eq(tickets)
    end

  end
end
