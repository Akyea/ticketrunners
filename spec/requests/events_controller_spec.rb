require 'rails_helper'

RSpec.describe 'Events', type: :request do

   describe 'GET index' do

     let!(:events) { create_list(:event, 10) }
     let(:user) { create(:user) }

     before { get '/api/events', params: { format: :json }, headers: { "X-User-Email": user.email } }

     it 'returns all events' do
       expect(json).not_to be_empty
       expect(json.size).to eq(10)
     end

     it 'returns status code 200' do
       expect(response).to have_http_status(:ok)
     end

   end

  describe 'GET show' do
    let(:event) { create(:event) }
    let(:tickets) { 4 }
    let(:user) { create(:user) }

    it 'renders event info' do
      get "/api/events/#{event.id}",
        params: { format: :json }, headers: { "X-User-Email": user.email }
        expect(response).to be_ok
        expect(json['id']).to eq(event.id)
        expect(json['name']).to eq(event.name)
        expect(json['venue']).to eq(event.venue)
        expect(json['alloted_tickets']).to eq(10)
        expect(json['tickets_sold']).to eq(0)
    end

    it 'checks the number of tickets available for an event' do
      post "/api/bookings/",
        params: {
          bookings: {
            event_id: event.id,
            number_of_tickets: tickets,
            amount: event.price,
            currency: 'EUR',
            token: :payment_done
          },
          format: :json
        },
        headers: {"X-User-Email": user.email}

      get "/api/events/#{event.id}/available-tickets",
        params: { format: :json },
        headers: { "X-User-Email": user.email }

        expect(response).to be_ok
        expect(json['available_tickets']).to eq(6)
    end

  end
end
