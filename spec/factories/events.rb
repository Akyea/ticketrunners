# frozen_string_literal: true

FactoryBot.define do
  factory :event do
    name { Faker::Movie.quote }
    venue { Faker::Address.street_name }
    starts_at { '2019-09-01 18:00:00' }
    alloted_tickets { 10 }
    tickets_sold { 0 }
    price { 200 }
  end
end
