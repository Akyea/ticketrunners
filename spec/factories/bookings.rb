# frozen_string_literal: true

FactoryBot.define do
  factory :booking do
    user
    event
    number_of_tickets { 3 }
  end
end
